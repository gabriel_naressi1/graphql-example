package resolver;

import com.coxautodev.graphql.tools.GraphQLRootResolver;
import domain.Pessoa;
import repository.PessoaRepository;

import java.util.List;

public class PessoaQueryResolver implements GraphQLRootResolver {

    private final PessoaRepository pessoaRepository;

    public PessoaQueryResolver(PessoaRepository pessoaRepository) {
        this.pessoaRepository = pessoaRepository;
    }

    public List<Pessoa> allPessoas() {
        return pessoaRepository.getAllPessoas();
    }

}