package resolver;

import com.coxautodev.graphql.tools.GraphQLRootResolver;
import domain.Pessoa;
import repository.PessoaRepository;

public class PessoaMutationResolver implements GraphQLRootResolver {

    private final PessoaRepository pessoaRepository;

    public PessoaMutationResolver(PessoaRepository pessoaRepository) {
        this.pessoaRepository = pessoaRepository;
    }

    public Pessoa createPessoa(String url, String description) {
        Pessoa newPessoa = new Pessoa(url, description);
        pessoaRepository.savePessoa(newPessoa);
        return newPessoa;
    }
}