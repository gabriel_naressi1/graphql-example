package repository;

import domain.Pessoa;

import java.util.ArrayList;
import java.util.List;

public class PessoaRepository {

    private List<Pessoa> pessoas = new ArrayList<>();

    public PessoaRepository(){

    }

    public List<Pessoa> getAllPessoas() {
        return pessoas;
    }

    public void savePessoa(Pessoa pessoa) {
        pessoas.add(pessoa);
    }
}