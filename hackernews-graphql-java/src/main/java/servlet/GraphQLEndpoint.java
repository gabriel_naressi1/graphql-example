package servlet;

import com.coxautodev.graphql.tools.SchemaParser;
import graphql.schema.GraphQLSchema;
import graphql.servlet.SimpleGraphQLServlet;
import repository.PessoaRepository;
import resolver.PessoaMutationResolver;
import resolver.PessoaQueryResolver;

import javax.servlet.annotation.WebServlet;


@WebServlet(urlPatterns = "/graphql")
public class GraphQLEndpoint extends SimpleGraphQLServlet {

    public GraphQLEndpoint() {
        super(buildSchema());
    }

    private static GraphQLSchema buildSchema() {
        PessoaRepository linkRepository = new PessoaRepository();
        return SchemaParser.newParser()
                .files("schema.graphqls")
                .resolvers(new PessoaQueryResolver(linkRepository), new PessoaMutationResolver(linkRepository))
                .build()
                .makeExecutableSchema();
    }
}